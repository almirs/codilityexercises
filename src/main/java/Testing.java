import java.util.Arrays;

public class Testing {


    public static void main(String[] args) {
        int A[] = {1,3};
        solution(A);
    }


    static int solution(int[] A) {
        int i=0;
        Arrays.sort(A);
        if(A.length == 1) {
            return 2;
        }

        if (A.length > 0 && A[0] !=1) {
            return 1;
        }
        while(i<A.length) {
            if(i == A.length - 1) {
                return  A[i] + 1;
            }
            if(A[i+1] > A[i] + 1) {
                return A[i] + 1;
            }
            i++;
        }
        return 1;
    }
}
