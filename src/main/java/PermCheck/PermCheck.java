package PermCheck;

import java.util.Arrays;

public class PermCheck {

    public static void main(String[] args) {
        int A[] = {1,1,1,1,1,1,1,1,1,1,1};
        int solution = solution(A);
        System.out.println(solution);
    }

     static int solution(int[] A) {
        // write your code in Java SE 8
        if(A.length<=1 && A[0] != 1) {
            return 0;
        }
        int i = 0;
        Arrays.sort(A);
        if(A[0] != 1) {
            return 0;
        }
        while (i < A.length) {
            if (i< A.length -1 && (A[i] == A[i + 1] || A[i]+1 != A[i+1])) {
                return 0;
            }
            i++;
        }
        return 1;
    }

}

