package FrogRiverOne;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FrogRiverOne {

    public static void main(String[] args) {
        int[] arr = {2, 2, 2, 2, 2};
        System.out.println(solution(2, arr));

    }

    static int solution(int X, int[] A) {
        boolean gotDestinationLocation = false;
        List<Integer> subArr = new ArrayList<Integer>();
        int i = 0;

        while (i < A.length) {
            subArr.add(A[i]);
            if (A[i] == X || gotDestinationLocation) {
                gotDestinationLocation = true;
                if (checkIfAllLeavesArePresent(subArr, X) == 1) {
                    return i;
                }

            }
            i++;
        }
        return -1;
    }

    static int checkIfAllLeavesArePresent(List<Integer> subArr, int x) {
        Collections.sort(subArr);
        for (int i = 1; i < subArr.size() || subArr.size() == 1; i++) {
            if(subArr.get(0) != 1) {
                return 0;
            }
            if (subArr.size() == 1 && (subArr.get(0) != 1)) {
                return 0;
            }  else if(subArr.size() == 1 && (subArr.get(0) == 1)) {
                return  1;
            } else if ((!subArr.get(i).equals(subArr.get(i - 1) + 1) && !subArr.get(i).equals(subArr.get(i - 1) ))) {
                return 0;
            }
        }
        return 1;
    }
}
